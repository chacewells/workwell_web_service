<?php
include_once 'ManagedObject.php';

class User implements ManagedObject {

	public $username;
	public $password;
	public $first_name;
	public $last_name;
	public $location;
	public $course;

	function __construct($username, $password, $first_name, $last_name, $location, $course) {
		$this->username = $username;
		$this->password = $password;
		$this->first_name = $first_name;
		$this->last_name = $last_name;
		$this->location = $location;
		$this->course = $course;
	}

	public function get_insert_statement() {
		$insert_string = <<<SQL
			INSERT INTO Users
			(
				username,
				password,
				first_name,
				last_name,
				location_id,
				course_id
			)
			VALUES
			(
				'{$this->username}',
				'{$this->password}',
				'{$this->first_name}',
				'{$this->last_name}',
				(SELECT id FROM Locations WHERE location_name = '{$this->location}'),
				(SELECT id FROM Courses WHERE course_name = '{$this->course}')
			)
SQL;
		return $insert_string;
	}

}

?>
