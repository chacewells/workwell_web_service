<?php
define('DEBUG', 1);
function get_mysqli() {
	$db_xml = file_get_contents('config/db_config.xml');
	$Connection = new SimpleXMLElement($db_xml);
	
	$mysqli = new mysqli($Connection->Host, $Connection->Username, $Connection->Password, $Connection->DatabaseName);
	
	if (DEBUG == 1) {
		if ($mysqli->connect_errno)
			echo "Error {$mysqli->connect_errno}: {$mysqli->connect_error}\n";
		else
			echo "success\n";
	}

	return $mysqli;
}
?>
